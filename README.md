# Docker Learning

## Docker Commands:
##### Run
It is used to run a container from an image.
for containers that execute a process like web servers it can be used along with the detach mode adding `-d` and the container name. If you want to attach use `docker attach ` + the ID of the container.

Interactive mode:
when an application requires to be interactive with the user, it must run with `-i`, also using `-t` it will interact with the terminal. Both commands can be used together `-it`.

Port Mapping:
A map to a port inside of the docker host cannot be reached directly outside the docker container, for that we use the `-p` option to map the port on the users machine and the docker container service. For example it can be used as: `docker run -p 80:5000` and the name of the image or container.

Volume Mapping:
All the changes in a file in a docker container happens just inside the docker container for this reason we could want to map those files in our own file system to persist the data. By running `docker run -v /my/folder:/var/my/folder` the folder route  var/my/folder will point to the my/route route

##### Ps
List all running containers and display useful information from them. To display all the containers including the ones that are not running we need the command `-a`.

##### Stop
It stops a running container. It requires a `container id` or the container `name`.

##### Rm
It removes a container . It requires a `container id` or the container `name`.

##### Images
List images and display their IDs and the sizes.

##### Rmi
Remove an image. It requires a `image id` or the image `name`.

##### Pull
Download an image.

##### Exec
It execute a command inside of a running container. It requires a `container id` or the container `name`.

##### Inspect a container
The `docker inspect` command followed by the container name or the id will return all the data of the container in JSON format.

##### Container Logs
Using the `docker logs` command followed by the container name or the id will display all the logs of the container.